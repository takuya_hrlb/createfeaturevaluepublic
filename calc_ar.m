function ar_feature = calc_ar(speed)

try
    ar_order = 10;
    y = speed;
    y = iddata(y);
    mb = ar(y, ar_order, 'ls');
    a = polydata(mb);
    ar_feature = a(2:end);
catch
    ar_feature = [];
end

end