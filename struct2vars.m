function struct2vars(variable)

structname = fieldnames(variable);
for i = 1:length(structname)
    assignin('caller', structname{i}, variable.(structname{i}));
end

end