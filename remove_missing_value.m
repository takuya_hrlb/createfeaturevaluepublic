function data = remove_missing_value(info, data)

struct2vars(info);

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_num = 1:num{i_gen} % num
            if isempty(data.(gen{i_gen})(i_num).(task{i_task}))
                continue
            end
            for i_level = 1:length(level) % level
                if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}), 'coding') == 0
                    continue
                end
                %                 if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding, 'no_me')
                %                     continue
                %                 end
                if isnan(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.RealTime(1))
                    continue
                end
                
                for i_segment = length(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all):-1:1
                    if isempty(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).ar_1)
                        data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment) = [];
                    end
                end
            end
        end
    end
end

end