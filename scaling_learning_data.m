function learning_data = scaling_learning_data(info, learning_data)

struct2vars(info);
segment_type = {'RT'; 'RNT'; 'no_error_with_touch'; 'no_error_without_touch'};
feature = fieldnames(learning_data.old.Breakfast.Standard.RT);
feature(strcmp('subject', feature)) = [];
feature(strcmp('type', feature)) = [];

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_level = 1:length(level) % level
            for i_segment_type = 1:length(segment_type) / 2
                for i_feature = 1:length(feature)
                    for i_count = 1:length({learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}).type})
                        learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type})(i_count).(feature{i_feature}) = (learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type})(i_count).(feature{i_feature}) - mean(cell2mat({learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}).(feature{i_feature})}))) / var(cell2mat({learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}).(feature{i_feature})}));
                    end
                end
            end
        end
    end
end