function data = select_segment(info, data)

struct2vars(info);

segment_type = {'RT'; 'RNT'; 'no_error_with_touch'; 'no_error_without_touch'};

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_num = 1:num{i_gen} % num
            if isempty(data.(gen{i_gen})(i_num).(task{i_task}))
                continue
            end
            for i_level = 1:length(level) % level
                if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}), 'coding') == 0
                    continue
                end
                %                 if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding, 'no_me')
                %                     continue
                %                 end
                if isnan(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.RealTime(1))
                    continue
                end
                for i_segment_type = 1:length(segment_type) / 2
                    if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment, segment_type{i_segment_type}) == 0
                        continue
                    end
                    idx = randsample(length(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.(segment_type{i_segment_type + 2})), length(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.(segment_type{i_segment_type})));
                    for i_select = 1:length(idx)
                        data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.select.(segment_type{i_segment_type + 2})(i_select) = data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.(segment_type{i_segment_type + 2})(idx(i_select));
                    end
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.select.(segment_type{i_segment_type}) = data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.(segment_type{i_segment_type});
                end
            end
        end
    end
end

end