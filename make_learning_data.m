function learning_data = make_learning_data(info, data)

struct2vars(info);
segment_type = {'RT'; 'RNT'; 'no_error_with_touch'; 'no_error_without_touch'};

learning_data = [];

for i_gen = 1:length(gen) % Y or O
    if isfield(learning_data, gen{i_gen}) == 0
        learning_data.(gen{i_gen}) = [];
        for i_task = 1:length(task) % breakfast or lunchbox
            if isfield(learning_data.(gen{i_gen}), task{i_task}) == 0
                learning_data.(gen{i_gen}).(task{i_task}) = [];
                for i_num = 1:num{i_gen} % num
                    if isempty(data.(gen{i_gen})(i_num).(task{i_task}))
                        continue
                    end
                    for i_level = 1:length(level) % level
                        if isfield(learning_data.(gen{i_gen}).(task{i_task}), level{i_level}) == 0
                            learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}) = [];
                        end
                        if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}), 'coding') == 0
                            continue
                        end
                        if isnan(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.RealTime(1))
                            continue
                        end
                        %                 if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding, 'no_me')
                        %                     continue
                        %                 end
                        if isnan(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.RealTime(1))
                            continue
                        end
                        for i_segment_type = 1:length(segment_type) / 2
                            if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment, segment_type{i_segment_type}) == 0
                                continue
                            end
                            if isfield(learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}), segment_type{i_segment_type}) == 0
                                learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}) = [];
                            end
                            learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}) = [learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}) data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.select.(segment_type{i_segment_type}) data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.select.(segment_type{i_segment_type + 2})];
                        end
                    end
                end
            end
        end
    end
end

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_level = 1:length(level) % level
            for i_segment_type = 1:length(segment_type) / 2
                learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}) = rmfield(learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}), 'subject');
                learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}) = rmfield(learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}), 'time_start');
                learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}) = rmfield(learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}), 'time_end');
                learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}) = rmfield(learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}), 'speed');
            end
        end
    end
end

end