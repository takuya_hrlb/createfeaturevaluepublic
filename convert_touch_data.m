function data = convert_touch_data(info, data)

struct2vars(info);

load ../rawDataSet.mat subjects;

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_level = 1:length(level) % standard or dual
            for i_num = 1:num{i_gen} % num
                if i_task == 1
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).touch.time = subjects(i_num).toastCoffee.vkc.touchFlag.t;
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).touch.flag = subjects(i_num).toastCoffee.vkc.touchFlag.flag;                 
                elseif i_task == 2
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).touch.time = subjects(i_num).lunchbox.vkc.touchFlag.t;
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).touch.flag = subjects(i_num).lunchbox.vkc.touchFlag.flag;                 
                end
            end
        end
    end
end

clearvars -except data

end