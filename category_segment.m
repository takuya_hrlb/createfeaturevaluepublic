function data = category_segment(info, data, varargin)

time = {'StartTime'; 'ErrorDetection'; 'ErrorCorrection'};
struct2vars(info);

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_num = 1:num{i_gen} % num
            if isempty(data.(gen{i_gen})(i_num).(task{i_task}))
                continue
            end
            for i_level = 1:length(level) % level
                if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}), 'coding') == 0
                    continue
                end
                %                 if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding, 'no_me')
                %                     continue
                %                 end
                if isnan(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.RealTime(1))
                    continue
                end
                zerocross = data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).time.zerocross;
                
                flag_around_me = 0;
                for i_segment = 1:length(zerocross) - 1
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).subject = i_num;
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).time_start = zerocross(i_segment);
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).time_end = zerocross(i_segment + 1);
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).speed = data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).speed(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.idx(i_segment):data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.idx(i_segment+1));
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).speed_mean = mean(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).speed);
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).speed_var = var(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).speed);
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).time_length = zerocross(i_segment + 1) - zerocross(i_segment);
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).speed_max = max(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).speed);
                    
                    ar_feature = calc_ar(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).speed);
                    for i_ar = 1:length(ar_feature)
                        data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).(['ar_', num2str(i_ar)]) = ar_feature(i_ar);
                    end
                    
                    if zerocross(i_segment, 4)
                        flag_around_me = 0;
                    end
                    if zerocross(i_segment, 2)
                        flag_around_me = 1;
                    end
                    if zerocross(i_segment + 1, 3)
                        idx_me =  find(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.ErrorDetection == zerocross(i_segment + 1));
                        if data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.Def2(idx_me) == 2
                            data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).type  = 'doubt_me';
                            continue
                        end
                        if data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.ErrorNumber(idx_me) == 1
                            data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).type  = 'RNT';
                        elseif data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.ErrorNumber(idx_me) == 2
                            data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).type  = 'RT';
                        end
                    else if flag_around_me
                            data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).type  = 'me_arround';
                        else
                            touch.time = data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).touch.time;
                            touch.flag = data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).touch.flag;
                            touch.status = touch.flag(find(zerocross(i_segment) < touch.time & touch.time < zerocross(i_segment + 1)));
                            if sum(touch.status) >= length(touch.status) / 2 % 半分以上タッチしていれば，タッチしていると判定
                                data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).type  = 'no_error_with_touch';
                            else
                                data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(i_segment).type  = 'no_error_without_touch';
                            end
                        end
                    end
                end
            end
        end
    end
    
    clearvars -except data;
    
end