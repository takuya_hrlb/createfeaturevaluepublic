function path = set_path(info, varargin)

nas = '/Volumes/VKproject/Temple_v1/Trimming/';
local = '../';

if isempty(varargin)
    current = local;
elseif strcmp(varargin{1}, 'nas')
    current = nas;
end

DB = vars2struct('info');
clearvars -except info DB;

elem.category = {'Speed'; 'Zerocross'; 'Coding'};
elem.path = {['APP/', elem.category{1}, '/']; ['APP/', elem.category{2}, '/']; [elem.category{3}, '/']};
dataset = 'rawDataSet.mat';

path.common = vars2struct('info', 'elem');
clearvars -except info path DB elem;

struct2vars(info);

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_level = 1:length(level) % standard or dual
            for i_num = 1:num{i_gen} % num
                for i_elem = 1:length(elem.category)
                    if i_elem == 3
                        continue
                    end
                    tmp.(lower(elem.category{i_elem})) = [DB.current, elem.path{i_elem}, task{i_task}, '/', elem.category{i_elem}, initial{i_gen}, num2str(i_num), '.csv'];
                end
                tmp.(lower(elem.category{i_elem})) = [DB.current, elem.path{i_elem}, task{i_task}, '/', level{i_level}, '/', elem.category{i_elem}, initial{i_gen}, num2str(i_num), '.xlsx'];
                path.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}) = tmp;
            end
        end
    end
end

path.common.elem = elem.path;
clearvars -except path

end