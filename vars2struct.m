function result = vars2struct(varargin)

vars = evalin('caller', 'who');
for i = 1:length(varargin)
    vars(find(strcmp(vars, varargin{i}))) = [];
end
vars(find(strcmp(vars, 'varargin'))) = [];
caller = repmat({'caller'}, size(vars));
result = cell2struct(cellfun(@evalin, caller, vars, 'UniformOutput', false), vars, 1);

end