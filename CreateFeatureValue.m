clc; clear;

load data.mat;
% info = set_info;
% path = set_path(info);
% data = set_data(info, path);
% data = convert_touch_data(info, data);
% data = fit_segment(info, data);
% data = category_segment(info, data);

% data = category_segment(info, data, 'no_ar');


data = remove_missing_value(info, data);
data = devide_segment_type(info, data);
data = select_segment(info, data);
learning_data = make_learning_data(info, data);
learning_data = scaling_learning_data(info, learning_data);
learning_data = convert_learning_data(info, learning_data);