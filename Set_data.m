function data = set_data(info, path)

elem.category = {'Speed'; 'Zerocross'; 'Coding'};
struct2vars(info);

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_level = 1:length(level) % standard or dual
            for i_num = 1:num{i_gen} % num
                for i_elem = 1:length(elem.category)
                    if i_elem == 3
                        continue
                    end
                    if exist(path.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).(lower(elem.category{i_elem}))) == 2
                        data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).(lower(elem.category{i_elem})) = csvread(path.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).(lower(elem.category{i_elem})));
                    end
                end
                if exist(path.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding) == 2
                    data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding = readtable(path.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).(lower(elem.category{i_elem})));
                    if isnan(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.RealTime(1))
                        data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.no_me(1) = 1;
                    end
                end
            end
        end
    end
end

clearvars -except data;

end