function info = set_info

gen = {'old'};
task = {'Breakfast'; 'LunchBox'};
level = {'Standard'};
initial = {upper(gen{1}(1))};
num = {14};

% gen = {'young'; 'old'};
% task = {'Breakfast'; 'LunchBox'};
% level = {'Standard'; 'Dual'};
% initial = {upper(gen{1}(1)); upper(gen{2}(1))};
% num = {21; 14};

info = vars2struct;

end



