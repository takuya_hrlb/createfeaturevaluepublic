function data = fit_segment(info, data)
time = {'StartTime'; 'ErrorDetection'; 'ErrorCorrection'};
struct2vars(info);

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_num = 1:num{i_gen} % num
            if isempty(data.(gen{i_gen})(i_num).(task{i_task}))
                continue
            end
            for i_level = 1:length(level) % level
                if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}), 'coding') == 0
                    continue
                end
                %                 if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding, 'no_me')
                %                     continue
                %                 end
                if isnan(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.StartTime(1))
                    continue
                end
                data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.idx = find(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).zerocross == 4);
                data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).time.zerocross = (data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.idx - 1) / 100;
                for i_time = 1:length(time)
                    for i_count = 1:height(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding)
                        idx = knnsearch(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).time.zerocross(:,1), data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.(time{i_time})(i_count));
                        data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.(time{i_time})(i_count) = data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).time.zerocross(idx, 1);
                        data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).time.zerocross(idx,i_time + 1) = 1;
                    end
                end
            end
        end
    end
end

clearvars -except data;

end