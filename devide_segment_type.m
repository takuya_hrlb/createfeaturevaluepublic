function data = devide_segment_type(info, data)

struct2vars(info);

segment_type = {'RT'; 'RNT'; 'no_error_with_touch'; 'no_error_without_touch'; 'me_arround'; 'doubt_me';};

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_num = 1:num{i_gen} % num
            if isempty(data.(gen{i_gen})(i_num).(task{i_task}))
                continue
            end
            for i_level = 1:length(level) % level
                if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}), 'coding') == 0
                    continue
                end
                %                 if isfield(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding, 'no_me')
                %                     continue
                %                 end
                if isnan(data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).coding.RealTime(1))
                    continue
                end
                for i_segment_type = 1:length(segment_type)
                    idx = find(strcmp(segment_type{i_segment_type}, {data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all.type}'));
                    for i_type = 1:length(idx)
                        data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.(segment_type{i_segment_type})(i_type) = data.(gen{i_gen})(i_num).(task{i_task}).(level{i_level}).segment.all(idx(i_type));
                    end
                end
            end
        end
    end
end

end