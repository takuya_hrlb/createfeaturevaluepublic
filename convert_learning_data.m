function learning_data = convert_learning_data(info, learning_data)

struct2vars(info);
segment_type = {'RT'; 'RNT'; 'no_error_with_touch'; 'no_error_without_touch'};

for i_gen = 1:length(gen) % Y or O
    for i_task = 1:length(task) % breakfast or lunchbox
        for i_level = 1:length(level) % level
            for i_segment_type = 1:length(segment_type) / 2
                learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}) = struct2table(learning_data.(gen{i_gen}).(task{i_task}).(level{i_level}).(segment_type{i_segment_type}));
            end
        end
    end
end

end